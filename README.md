# InkStep XpubConversion

This is a step gem that is meant to run as a plugin for [the Ink API](https://gitlab.coko.foundation/INK/ink-api).

## Installation
 
See [the Ink documentation](https://gitlab.coko.foundation/INK/ink-api) for detailed instructions for installing this plugin into an instance of INK.  

## Tests 

Run `bundle exec rspec` to execute the test suite

## Steps included:

### UnzipStep

Extracts a .zip file into the current working directory and deletes the .zip file

### LatexToPdfStep

Compiles a set of LaTeX source files into a pdf using `pdflatex` 

