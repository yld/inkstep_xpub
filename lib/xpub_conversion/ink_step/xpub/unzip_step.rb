require 'xpub_conversion/version'
require 'ink_step/conversion_step'
require 'ink_step/mixins/zip_methods'

module InkStep::Xpub
  class UnzipStep < InkStep::ConversionStep
    include InkStep::Mixins::ZipMethods

    def perform_step
      super
      source_file_name = find_source_file(regex: [/\.zip$/])
      source_file_path = File.join(working_directory, source_file_name)
      log_as_step "Unzipping #{source_file_path}"
      unzip_archive(source_file_path)
      FileUtils.cp_r(File.join(unzip_directory, "."), working_directory)
      FileUtils.rm_rf(unzip_directory)
      FileUtils.rm_rf(source_file_path)
      success!
    end

    def version
      XpubConversion::VERSION
    end

    def self.description
      "Extract files from a .zip file into the working directory and delete the .zip file"
    end

    def self.human_readable_name
      "Unzip"
    end

  end
end
