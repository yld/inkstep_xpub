require 'xpub_conversion/version'
require 'ink_step/conversion_step'

module InkStep::Xpub
  class LatexToPdfStep < InkStep::ConversionStep

    def perform_step
      super
      source_file_name = find_source_file(regex: [/\.tex$/])
      source_file_path = File.join(working_directory, source_file_name)
      log_as_step "Converting #{source_file_path} with pdflatex"
      perform_latex_conversion(source_file_path)
      success!
    end

    def version
      XpubConversion::VERSION
    end

    def self.description
      "Convert latex to pdf with pdftex"
    end

    def self.human_readable_name
      "Latex to pdf"
    end

    def perform_latex_conversion(source_file_path)
      command = "pdflatex #{Shellwords.escape(source_file_path)} -halt-on-error"
      perform_command(command: command, error_prefix: "Problem running latex conversion")
    end
  end
end
