module XpubConversion
  if defined?(Rails)
    require 'xpub_conversion/engine'
  else
    require 'xpub_conversion/ink_step/xpub/unzip_step'
    require 'xpub_conversion/ink_step/xpub/latex_to_pdf_step'
  end
end
