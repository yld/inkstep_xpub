# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'xpub_conversion/version'

Gem::Specification.new do |spec|
  spec.name           = 'inkstep_xpub_conversion'
  spec.version        = XpubConversion::VERSION
  spec.date           = '2017-10-30'
  spec.summary        = "Convert latex to pdf"
  spec.description    = "Uses pdftex" 
  spec.authors        = ["Sam Galson"]
  spec.email          = 'sam.galson@yld.io'
  spec.homepage       = 'https://gitlab.coko.foundation/yld/inkstep_xpub'
  spec.license        = 'MIT'

  spec.executables    = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files     = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths  = %w(lib)

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "pdf-reader"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "rake"

  spec.required_ruby_version = '~> 2.2'
  spec.files         = Dir.glob("{lib}/**/*") + %w(./README.md)
end
