require 'spec_helper'
require 'xpub_conversion/ink_step/xpub/latex_to_pdf_step'
require 'pdf-reader'

describe InkStep::Xpub::LatexToPdfStep do
  subject               { InkStep::Xpub::LatexToPdfStep.new(chain_file_location: temp_directory, position: 1) }

  let(:target_files)           { File.join(Dir.pwd, "spec", "fixtures", "elife-template", ".") }
  let!(:input_directory)    { File.join(temp_directory, InkStep::Base::INPUT_FILE_DIRECTORY_NAME) }

  before do
    create_directory_if_needed(input_directory)
    FileUtils.cp_r(target_files, input_directory)
  end

  after :each do
    FileUtils.rm_rf(FileUtils.rm_rf(Dir.glob("#{temp_directory}/*")))
  end

  describe '#perform_step' do
    before do
      create_directory_if_needed(input_directory)
      FileUtils.cp_r(target_files, input_directory)
    end

    context 'when converting Tex files' do

      it 'returns a compiled pdf' do
        working_directory = subject.send(:working_directory)
        Dir.chdir(working_directory) do
          subject.perform_step
        end

        reader = PDF::Reader.new(File.join(working_directory, "elife-template.pdf"))
        first_page = reader.page(1).text
        expect(first_page).to include 'Manuscript submitted to eLife' 
      end
    end
  end

  describe '#version' do
    specify do
      expect{subject.version}.to_not raise_error
    end
  end

  describe '#description' do
    specify do
      expect{subject.class.description}.to_not raise_error
    end
  end

  describe '#human_readable_name' do
    specify do
      expect{subject.class.human_readable_name}.to_not raise_error
    end
  end
end
