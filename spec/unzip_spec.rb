require 'spec_helper'
require 'xpub_conversion/ink_step/xpub/unzip_step'

describe InkStep::Xpub::UnzipStep do
  subject               { InkStep::Xpub::UnzipStep.new(chain_file_location: temp_directory, position: 1) }

  let(:target_file)           { File.join(Dir.pwd, "spec", "fixtures", "elife-template.zip") }
  let!(:input_directory)    { File.join(temp_directory, InkStep::Base::INPUT_FILE_DIRECTORY_NAME) }

  before do
    create_directory_if_needed(input_directory)
    FileUtils.cp(target_file, input_directory)
  end

  after :each do
    FileUtils.rm_rf(FileUtils.rm_rf(Dir.glob("#{temp_directory}/*")))
  end

  describe '#perform_step' do
    before do
      create_directory_if_needed(input_directory)
      FileUtils.cp(target_file, input_directory)
    end

    context 'when converting Tex files' do

      it 'returns a compiled pdf' do
        working_directory = subject.send(:working_directory)
        subject.perform_step
        actual = Dir[File.join(working_directory, '*')].map { |file| File.basename(file) }
        expected = Dir[File.join(Dir.pwd, "spec", "fixtures", "elife-template", "*")].map { |file| File.basename(file) }
        expect(actual).to eq expected
      end
    end
  end

  describe '#version' do
    specify do
      expect{subject.version}.to_not raise_error
    end
  end

  describe '#description' do
    specify do
      expect{subject.class.description}.to_not raise_error
    end
  end

  describe '#human_readable_name' do
    specify do
      expect{subject.class.human_readable_name}.to_not raise_error
    end
  end
end
